using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform player;  // Referencia al transform del jugador
    [SerializeField] float offsetX = 0f;  // Offset en el eje X (puedes ajustarlo si quieres)

    void LateUpdate()
    {
        // Mantener la posici�n X fija, seguir al jugador en Y y Z
        if (player.position.y < 2)
            return;
        Vector3 newPosition = new Vector3(offsetX, player.position.y, transform.position.z);
        transform.position = newPosition;
    }
}
