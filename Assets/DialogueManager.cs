using UnityEngine;
using TMPro;
using System.Collections;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI dialogueText; // Referencia al componente TextMeshPro
    [SerializeField] private GameObject textBoxPanel; // Referencia al panel del TextBox

    private string[] dialogueArray = {
        "So another has fallen into the well. Are you okay? Can you move?",
        "Try moving around your arms with      and   . . Maybe even grabbing ledges with      and      .",
        "That one looks more stable! Try jumping with       while keeping both hands on the ledge."
    };
    private int dialogueIndex = 0;

    private bool isDialogueActive = false;
    private Coroutine dialogueCoroutine;
    [SerializeField] GameObject case2;
    [SerializeField] GameObject case3;
    [SerializeField] GameObject trigger;
    private GameObject currentTrigger;

    private void Start()
    {
        textBoxPanel.SetActive(false);
        case2.SetActive(false);
        case3.SetActive(false);
    }

    // M�todo para cambiar el texto
    public void ShowNextDialogue()
    {
        if (!isDialogueActive)
        {
            if (dialogueIndex < 2)
            {
                dialogueCoroutine = StartCoroutine(DisplayDialogues(2 - dialogueIndex));
            }
            else if (dialogueIndex < dialogueArray.Length)
            {
                dialogueCoroutine = StartCoroutine(DisplaySingleDialogue());
            }
        }
    }

    private IEnumerator DisplayDialogues(int numberOfDialogues)
    {
        isDialogueActive = true;
        Debug.Log(dialogueIndex);

        for (int i = 0; i < numberOfDialogues && dialogueIndex < dialogueArray.Length; i++)
        {
            textBoxPanel.SetActive(true);
            dialogueText.text = dialogueArray[dialogueIndex];

            if (dialogueIndex == 0)
            {
                yield return new WaitForSeconds(0.3f);
                // sonido 
                FindObjectOfType<AudioManager>().Play("malegrunt");
                yield return new WaitForSeconds(5f);
            }
            else if (dialogueIndex == 1)
            {
                case2.SetActive(true);
                FindObjectOfType<AudioManager>().Play("malegrunt2");
                yield return new WaitForSeconds(10f);
            }
            else if (dialogueIndex == 2)
            {
                case2.SetActive(false);
                case3.SetActive(true);
            }
            //else if (dialogueIndex == 3)    -----   est� comentado porque por ahora no hay dialogueArray [3]
            //{
            //    case3.SetActive(false);
            //}



            //switch (dialogueIndex)
            //{
            //    case 1:
            //        case2.SetActive(true);
            //        break;
            //    case 2:
            //        case2.SetActive(false);
            //        case3.SetActive(true);
            //        break;
            //    case 3:
            //        case3.SetActive(false);
            //        break;
            //}

            dialogueIndex++;

            textBoxPanel.SetActive(false);
            dialogueText.text = "";
        }

        //if (dialogueIndex >= 2 && currentTrigger != null)
        //{
            //Destroy(currentTrigger);
            trigger.SetActive(false);
        //}

        

        isDialogueActive = false;
    }

    private IEnumerator DisplaySingleDialogue()
    {
        isDialogueActive = true;

        if (dialogueIndex < dialogueArray.Length)
        {
            textBoxPanel.SetActive(true);
            dialogueText.text = dialogueArray[dialogueIndex];

            if (dialogueIndex == 1)
            {
                case2.SetActive(true);
            }
            else if (dialogueIndex == 2)
            {
                case2.SetActive(false);
                case3.SetActive(true);
                FindObjectOfType<AudioManager>().Play("malegrunt3");
                yield return new WaitForSeconds(10f);
            }
            //else if (dialogueIndex == 3)
            //{
            //    case3.SetActive(false);
            //}

            dialogueIndex++;

            //yield return new WaitForSeconds(10f);

            textBoxPanel.SetActive(false);
            dialogueText.text = "";
        }

        isDialogueActive = false;
    }
}
