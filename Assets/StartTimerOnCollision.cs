using System;
using UnityEngine;

public class StartTimerOnCollision : MonoBehaviour
{
    [SerializeField] Timer timerScript;
    [SerializeField] RigidBodyMovement rigidBodyMovement;
    private void Start()
    {
        rigidBodyMovement.stopTimer = StopTime;
    }

    private void StopTime()
    {
        timerScript.StopTimer();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (rigidBodyMovement.hasStarted == true)//&& collision.tag == "Pivot")
        {
            timerScript.StartTimer();
        }
    }
}
