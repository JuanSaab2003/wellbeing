using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpwardsMovement : MonoBehaviour
{
    Rigidbody2D rb;
    bool enable = true;
    bool enableCount = true;
    public TextMeshProUGUI time;
    public TextMeshProUGUI title;

    float count = 0;
    float timerAppear = 4;
    float titleAppear = 3;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        time.text = PlayerPrefs.GetString("time");
    }
    void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        if(enableCount)
            count += Time.deltaTime;
        if (enable)
        {
            Vector2 velocity = new Vector2(rb.velocity.x, 15);
            rb.velocity = velocity;
        }
        float newPosition = Camera.main.transform.position.y - 0.5f * Time.deltaTime;
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, newPosition, Camera.main.transform.position.z);
        if (Camera.main.transform.position.y <= 3)
        {
            rb.bodyType = RigidbodyType2D.Static;
            enable = false;
            Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, 23, Camera.main.transform.position.z);
        }
        if(count >= titleAppear)
        {
            title.gameObject.SetActive(true);
        }
        if (count >= timerAppear)
        {
            time.gameObject.SetActive(true);
            enableCount = false;
        }
    }
}
