using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.IK;

public class BodyPartMovement : MonoBehaviour
{
    public GameObject
        Head,
        Body,
        L_Arm,
        L_Forearm,
        L_Hand,
        R_Arm,
        R_Forearm,
        R_Hand,
        L_Thigh,
        L_Leg,
        R_Thigh,
        R_Leg;
    public GameObject Pivot;
    public bool lockedRight = false;
    public bool lockedLeft = false;
    private void Update()
    {        
        if(Input.GetKey("q"))
        {
            LimbRotation(L_Arm, -1, Camera.main.ScreenToWorldPoint(Input.mousePosition), 89, 260, ref lockedLeft);
            ForeArmRotation(L_Forearm, -1, Pivot.transform.position);
        }
        if (Input.GetKey("e"))
        {
            LimbRotation(R_Arm, 1, Camera.main.ScreenToWorldPoint(Input.mousePosition), 100, 271, ref lockedRight);
            ForeArmRotation(R_Forearm, 1, Pivot.transform.position);
        }
    }

    private void ForeArmRotation(GameObject Limb, int multiplier, Vector3 pivot)
    {
        Vector2 positionDifference = pivot - Limb.transform.position;
        float degrees = -Mathf.Atan2(positionDifference.x, positionDifference.y) * 180f / Mathf.PI;
        degrees += multiplier*90;
        Limb.transform.rotation = Quaternion.Euler(0, 0, degrees);
        if (-Limb.transform.localRotation.w * multiplier <= 0 && Limb.transform.localRotation.z <= 0)
        {
            Limb.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void LimbRotation(GameObject Limb, float multiplier, Vector3 pivot, float min, float max, ref bool locked)
    {
        Vector2 positionDifference = pivot - Limb.transform.position;
        float degrees = -Mathf.Atan2(positionDifference.x, positionDifference.y) * 180f / Mathf.PI;
        degrees += multiplier*90;
        if (degrees < 0)
            degrees += 360;
        if (CheckRotation(Limb, degrees, min, max, ref locked))
        {
            locked = false;
            Limb.transform.rotation = Quaternion.Euler(0, 0, degrees);
        }
    }

    private bool CheckRotation(GameObject Limb, float degrees, float min, float max, ref bool locked)
    {
        if (degrees < max && degrees > min && locked)
        {
            return false;
        }
        else if (degrees < max && degrees > 180)
        {
            locked = true;
            Limb.transform.rotation = Quaternion.Euler(0, 0, max);
            return false;
        }
        else if (degrees > min && degrees < 180)
        {
            locked = true;
            Limb.transform.rotation = Quaternion.Euler(0, 0, min);
            return false;
        }
        return true;
    }
}
