using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RigidBodyMovement : MonoBehaviour
{
	[SerializeField]
	HandChangeL handChangeL;

	[SerializeField]
	HandChangeR handChangeR;

	public Rigidbody2D rb_torso;

    public Rigidbody2D L_rb_hand;
    public Rigidbody2D L_rb_forearm;
    public Rigidbody2D L_rb_arm;

    public Rigidbody2D R_rb_hand;
    public Rigidbody2D R_rb_forearm;
    public Rigidbody2D R_rb_arm;

    public Dictionary<string, PlayerArm> bodyParts;

    public TriggerEnter R_trigger;
    public TriggerEnter L_trigger;
    public HeadCollision head_trigger;

    public ColliderLeg R_collider;
    public ColliderLeg L_collider;

    public bool isMoving_R = false, isMoving_L = false;
    private const string RIGHT = "R", LEFT = "L";

    public bool hasStarted = false;

    public float armForce;
    public float torsoForce;

    private PlayerInput playerInput;

    public Image BlackBackground;

    public Rigidbody2D bucketRigidBody;
    public Action stopTimer;
    private void Start()
    {
        rb_torso.bodyType = RigidbodyType2D.Dynamic;

        L_rb_hand.bodyType = RigidbodyType2D.Dynamic;
        L_rb_forearm.bodyType = RigidbodyType2D.Dynamic;
        L_rb_arm.bodyType = RigidbodyType2D.Dynamic;

        R_rb_hand.bodyType = RigidbodyType2D.Dynamic;
        R_rb_forearm.bodyType = RigidbodyType2D.Dynamic;
        R_rb_arm.bodyType = RigidbodyType2D.Dynamic;

        bodyParts = new Dictionary<string, PlayerArm>()
        {
            {RIGHT, new PlayerArm(R_rb_hand, R_rb_forearm, R_rb_arm)},
            {LEFT, new PlayerArm(L_rb_hand, L_rb_forearm, L_rb_arm)}
        };

        R_trigger.pivotStatus = SetPivotStatus;
        L_trigger.pivotStatus = SetPivotStatus;

        R_trigger.jumpable = SetJumpable;
        L_trigger.jumpable = SetJumpable;

        R_trigger.rockCollision = RockBottom;
        L_trigger.rockCollision = RockBottom;
        head_trigger.rockCollision = RockBottom;

        R_trigger.bucketReached = SetBucketStatus;
        L_trigger.bucketReached = SetBucketStatus;

        R_collider.floorStatus = SetOnTheFloor;
        L_collider.floorStatus = SetOnTheFloor;
    }

    private void SetBucketStatus(bool val, string hand)
    {
        bodyParts[hand].bucket = val;
    }

    private void RockBottom()
    {
        bodyParts[LEFT].Hand.bodyType = RigidbodyType2D.Dynamic;
        bodyParts[RIGHT].Hand.bodyType = RigidbodyType2D.Dynamic;
    }

    private void SetJumpable(bool val, string hand)
    {
        bodyParts[hand].onJumpable = val;
    }
    private void Update()
    {
        if ((bodyParts[LEFT].bucket && bodyParts[LEFT].Hand.bodyType == RigidbodyType2D.Static) || (bodyParts[RIGHT].bucket && bodyParts[RIGHT].Hand.bodyType == RigidbodyType2D.Static))
        {
            stopTimer.Invoke();
            bucketRigidBody.bodyType = RigidbodyType2D.Static;
            GetComponent<PlayerInput>().actions = null;
            StartCoroutine(FadeOut());
        }
    }
    private IEnumerator FadeOut()
    {
        float counter = 0;
        float duration = 2;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(0, 1, counter / duration);
            BlackBackground.color = new Color(BlackBackground.color.r, BlackBackground.color.g, BlackBackground.color.b, alpha);
            yield return null;
        }
        SceneManager.LoadScene(2);
    }
    private void MoveArm(PlayerArm playerArm, Vector2 direction)
    {
        rb_torso.bodyType = RigidbodyType2D.Dynamic;
        playerArm.Hand.bodyType = RigidbodyType2D.Dynamic;
        direction = new Vector2(direction.x / 2, direction.y);
        if ((bodyParts[LEFT].Hand.bodyType == RigidbodyType2D.Static && bodyParts[RIGHT].Hand.bodyType == RigidbodyType2D.Dynamic) ||
            (bodyParts[LEFT].Hand.bodyType == RigidbodyType2D.Dynamic && bodyParts[RIGHT].Hand.bodyType == RigidbodyType2D.Static) ||
            bodyParts[LEFT].onFloor || bodyParts[RIGHT].onFloor)
        {
            playerArm.Hand.velocity = direction * armForce;
        }
        else if (!isMoving_L && !isMoving_R)
        {
            direction.y = playerArm.Hand.velocity.y;
            direction.x *= 2;
            playerArm.Hand.velocity = direction;
        }
    }
    private void StopArm(PlayerArm playerArm)
    {
        bodyParts[LEFT].Hand.constraints = RigidbodyConstraints2D.FreezeRotation;
        bodyParts[RIGHT].Hand.constraints = RigidbodyConstraints2D.FreezeRotation;

        playerArm.Hand.bodyType = RigidbodyType2D.Static;
    }
    private void SetPivotStatus(bool val, string hand)
    {
        bodyParts[hand].onPivot = val;
    }
    private void SetOnTheFloor(bool val)
    {
        bodyParts[RIGHT].onFloor = val;
        bodyParts[LEFT].onFloor = val;
    }
    private void OnLeftGrip()
    {
        isMoving_L = !isMoving_L;
        if (isMoving_L && bodyParts[LEFT].onPivot)
        {
            hasStarted = true;
            handChangeL.greenHand = true;
			handChangeL.yellowHand = false;
			handChangeL.redHand = false;
			StopArm(bodyParts[LEFT]);
			FindObjectOfType<AudioManager>().Play("sound2");
		}
		else if (!isMoving_L)
		{
			handChangeL.greenHand = false;
			handChangeL.yellowHand = false;
			handChangeL.redHand = true;
			rb_torso.bodyType = RigidbodyType2D.Dynamic;
			bodyParts[LEFT].Hand.bodyType = RigidbodyType2D.Dynamic;
		}
		else
        {
			handChangeL.greenHand = false;
			handChangeL.yellowHand = true;
			handChangeL.redHand = false;
			rb_torso.bodyType = RigidbodyType2D.Dynamic;
            bodyParts[LEFT].Hand.bodyType = RigidbodyType2D.Dynamic;
        }
    }
    private void OnRightGrip()
    {
        isMoving_R = !isMoving_R;
        if (isMoving_R && bodyParts[RIGHT].onPivot)
        {
            hasStarted = true;
			handChangeR.greenHand = true;
			handChangeR.yellowHand = false;
			handChangeR.redHand = false;
			StopArm(bodyParts[RIGHT]);
			FindObjectOfType<AudioManager>().Play("sound2");

		}
		else if (!isMoving_R)
		{
			handChangeR.greenHand = false;
			handChangeR.yellowHand = false;
			handChangeR.redHand = true;
			rb_torso.bodyType = RigidbodyType2D.Dynamic;
			bodyParts[RIGHT].Hand.bodyType = RigidbodyType2D.Dynamic;
		}
		else
        {
			handChangeR.greenHand = false;
			handChangeR.yellowHand = true;
			handChangeR.redHand = false;
			rb_torso.bodyType = RigidbodyType2D.Dynamic;
            bodyParts[RIGHT].Hand.bodyType = RigidbodyType2D.Dynamic;
        }
    }
    private void OnLeftMove(InputValue value)
    {
        if (!isMoving_L || (bodyParts[LEFT].onFloor && !isMoving_L))
        {
            //hasStarted = true;
            MoveArm(bodyParts[LEFT], value.Get<Vector2>().normalized);
        }
    }
    private void OnRightMove(InputValue value)
    {
        if (!isMoving_R || (bodyParts[RIGHT].onFloor && !isMoving_R))
        {
            MoveArm(bodyParts[RIGHT], value.Get<Vector2>().normalized);
        }
    }
    private void OnJump(InputValue value)
    {
        if (bodyParts[RIGHT].Hand.bodyType == RigidbodyType2D.Static &&
            bodyParts[LEFT].Hand.bodyType == RigidbodyType2D.Static &&
            bodyParts[RIGHT].onJumpable &&
            bodyParts[LEFT].onJumpable)
        {
            bodyParts[LEFT].onJumpable = false;
            bodyParts[RIGHT].onJumpable = false;

            bodyParts[LEFT].Hand.bodyType = RigidbodyType2D.Dynamic;
            bodyParts[RIGHT].Hand.bodyType = RigidbodyType2D.Dynamic;
            rb_torso.bodyType = RigidbodyType2D.Dynamic;

            bodyParts[LEFT].Hand.AddForce(Vector2.up * 9000);
            bodyParts[RIGHT].Hand.AddForce(Vector2.up * 9000);
        }
    }
}
public class PlayerArm
{
    public Rigidbody2D Hand;
    public Rigidbody2D ForeArm;
    public Rigidbody2D Arm;
    public bool onFloor;
    public bool onPivot;
    public bool onJumpable;
    public bool onSliding;
    public bool bucket;
    public PlayerArm(Rigidbody2D Hand, Rigidbody2D ForeArm, Rigidbody2D Arm)
    {
        this.Hand = Hand;
        this.ForeArm = ForeArm;
        this.Arm = Arm;
        onFloor = false;
        onPivot = false;
        onSliding = false;
        onJumpable = false;
        bucket = false;
    }
}
