using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockScript : MonoBehaviour
{
    public Action<RockScript> CollisionAction;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == null) return;
        if(collision.collider.tag == "Player")
            CollisionAction.Invoke(this);
    }
}
