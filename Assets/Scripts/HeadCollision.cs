using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HeadCollision : MonoBehaviour
{
    public Action rockCollision;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == null) return;
        if(collision.collider.tag == "rock")
        {
            rockCollision.Invoke();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "ActivateSpawner")
        {
            collision.gameObject.GetComponent<SpawnerTrigger>().rockSpawner.spawningInactive = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "ActivateSpawner")
        {
            collision.gameObject.GetComponent<SpawnerTrigger>().rockSpawner.spawningInactive = true;
        }
    }
}
