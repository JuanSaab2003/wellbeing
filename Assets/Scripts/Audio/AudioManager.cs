using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

/*
To add any audio, go to the AudioManager game object and, in the Sounds list, add your audio file and give the sound a name.

To play audio from any script, simply write the following line:
FindObjectOfType<AudioManager>().Play("sound name as it is in the audio manager (inside quotation marks)");
*/

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    public static AudioManager instance;

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sn in sounds)
        {
            sn.source = gameObject.AddComponent<AudioSource>();

            sn.source.clip = sn.clip;
            sn.source.volume = sn.volume;
            sn.source.pitch = sn.pitch;
            sn.source.loop = sn.loop;
        }

    }

    public void Play(string name)
    {
        Sound snd = Array.Find(sounds, sounds => sounds.name == name);

        if (snd == null)
        {
            Debug.Log("No se ha encontrado el sonido" + name);
            return;
        }
        snd.source.Play();
    }

}
