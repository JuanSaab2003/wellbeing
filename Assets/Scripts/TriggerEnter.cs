using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnter : MonoBehaviour
{
    public Action<bool, string> pivotStatus;
    public Action<bool, string> jumpable;
    public Action<bool, string> slidable;
    public Action<bool, string> bucketReached;
    public Action rockCollision;
    public string Hand = "";
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider == null) return;
        if(collision.collider.tag == "rock")
        {
            rockCollision.Invoke();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pivot")
        {
            jumpable.Invoke(false, Hand);
            pivotStatus.Invoke(true, Hand);
        }
        if (collision.tag == "Ledge")
        {
            pivotStatus.Invoke(true, Hand);
            jumpable.Invoke(true, Hand);
        }
        if(collision.tag == "BUCKET")
        {
            pivotStatus.Invoke(true, Hand);
            bucketReached.Invoke(true, Hand);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Pivot")
        {
            pivotStatus.Invoke(false, Hand);
        }
        if (collision.tag == "Ledge")
        {
            pivotStatus.Invoke(false, Hand);
        }
        if(collision.tag == "BUCKET")
        {
            pivotStatus.Invoke(false, Hand);
            bucketReached.Invoke(false, Hand);
        }
    }
}
