using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderLeg : MonoBehaviour
{
    public Action<bool> floorStatus;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        floorStatus.Invoke(true);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        floorStatus.Invoke(false);
    }
}
