using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScroller : MonoBehaviour
{
    [SerializeField] private float speed;
    private Vector3 StartPosition;

    private void Start()
    {
        StartPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
        if(transform.position.y > 12f)
        {
            transform.position = StartPosition;
        }
    }
}
