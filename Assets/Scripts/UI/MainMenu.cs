using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        Destroy(GameObject.Find("audioManager"));
        SceneManager.LoadScene("LevelScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        //if(SceneManager.GetActiveScene() != "LevelScene")
        //{

        //}
    }
}
