using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timer;

    private float elapsedTime;
    private bool isRunning = false;

    void Start()
    {
        elapsedTime = 0f;
        timer.text = FormatTime(elapsedTime);
    }

    void Update()
    {
        if (isRunning)
        {
            elapsedTime += Time.deltaTime;
            timer.text = FormatTime(elapsedTime);
        }
    }
    public void StopTimer()
    {
        isRunning = false;
        PlayerPrefs.SetString("time", timer.text);
    }
    public void StartTimer()
    {
        if (!isRunning)
        {
            isRunning = true;
        }
    }

    string FormatTime(float time)
    {
        int intTime = (int)time;
        int minutes = intTime / 60;
        int seconds = intTime % 60;
        float fraction = time * 1000;
        fraction = (fraction % 1000);
        string timeText = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
        return timeText;
    }
}
