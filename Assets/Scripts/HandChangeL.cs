using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandChangeL : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite[] newSprite;

    public bool redHand;
	public bool yellowHand;
	public bool greenHand;


	public void changeSprite(int spriteNumber)
    { 
        spriteRenderer.sprite = newSprite[spriteNumber];
    }

	private void Start()
	{
        redHand = true;
        yellowHand = false;
        greenHand = false;
	}

	private void Update()
	{
        if (redHand)
        {
            changeSprite(0);
        }
        else if (yellowHand)
        {
            changeSprite(1);
        }
        else if (greenHand)
        {
            changeSprite(2);
        }

	}
}
