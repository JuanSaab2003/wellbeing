using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSwinging : MonoBehaviour
{
    public Rigidbody2D rope;
    public float MaximumAllowed = 2.75f;
    public float Xmax, Ymax;
    private void Start()
    {
        rope = GetComponent<Rigidbody2D>();
        //rope.velocity = new Vector2(-3, 0);
    }
    private void Update()
    {
        if (rope.velocity.x <= 0 && rope.velocity.y <= 0 && rope.velocity.y >= -0.25f && Mathf.Abs(rope.velocity.x) < 3f)
        {
            rope.AddForce(Vector2.left * 3);
        }
        if(rope.velocity.x > MaximumAllowed)
        {
            rope.velocity = new Vector2(MaximumAllowed, rope.velocity.y);
            Xmax = rope.velocity.x;
        }
    }
}
