using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FallingRocks : MonoBehaviour
{
    public List<RockScript> RockPrefab;
    float counter = 0;
    public float showTime = 1f;
    public float max_count = 5f;
    public float minusScreenHeight = 50;
    public bool spawningInactive = true;

    public Image iconImage;
    public float blinkInterval = 0.1f;

    private void Start()
    {
        iconImage.gameObject.SetActive(false);
    }

    private void ActivateSpawner(bool val)
    {
        spawningInactive = val;
    }

    private void Update()
    {
        if (spawningInactive)
            return;
        if (counter <= 0)
        {
            float randomDeviation = UnityEngine.Random.Range(-0.25f, 0.25f);
            int randomRock = UnityEngine.Random.Range(0, RockPrefab.Count);
            Vector3 position = new Vector3(transform.position.x + randomDeviation, transform.position.y, transform.position.z);
            RockScript rock = Instantiate(RockPrefab[randomRock], position, Quaternion.Euler(new Vector3(0, 0, -40)));
            rock.CollisionAction = MakeFallThrough;
            counter = max_count;

            ShowIcon(transform.position);
        }
        else
        {
            counter -= Time.deltaTime;
        }
    }

    private void MakeFallThrough(RockScript rock)
    {
        StartCoroutine(Wait(rock));
    }

    private IEnumerator Wait(RockScript rock)
    {
        yield return new WaitForSeconds(0.5f);
        rock.GetComponent<Collider2D>().enabled = false;
    }

    private void ShowIcon(Vector3 rockPosition)
    {
        iconImage.gameObject.SetActive(true);

        Vector3 screenPosition = Camera.main.WorldToScreenPoint(rockPosition);
        float screenX = screenPosition.x;
        float fixedScreenY = Screen.height - minusScreenHeight;

        iconImage.transform.position = new Vector3(screenX, fixedScreenY, 0);

        StartCoroutine(BlinkIcon(showTime));
    }

    private IEnumerator BlinkIcon(float duration)
    {
        float endTime = Time.time + duration;
        bool isVisible = true;

        while (Time.time < endTime)
        {
            iconImage.color = new Color(iconImage.color.r, iconImage.color.g, iconImage.color.b, isVisible ? 1f : 0f);
            isVisible = !isVisible;
            yield return new WaitForSeconds(blinkInterval);
        }

        iconImage.gameObject.SetActive(false);
    }
}
